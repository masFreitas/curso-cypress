describe("Tickets", () => {

    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the input test fields", () => {
        const firstName = "Mateus";
        const lastName = "Freitas";

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type("mateus@testing.com");
        cy.get('#requests').type("Qualquercoisa");
        cy.get('#signature').type(`${firstName} ${lastName}`);
    });

    it("select 2 tickets", () => {
        cy.get('#ticket-quantity').select("2");
    });

    it("select vip  ticket type", () => {
        cy.get('#vip').check();
    })

    it("selects social media checkbox", () => {
        cy.get('#social-media').check();
    });

    it("selects friend and publication, then uncheck friend", () => {
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get('#friend').uncheck();
    });

    it("Has 'TICKETBOX' headers heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alert on invalid email", () => {
        cy.get('#email')
          .as("email")
          .type("mateus-testing.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
          .clear()
          .type("mateus@testing.com");

          cy.get("#email.invalid").should("not.exist");
    });

    it("fill and reset the form", () => {
        const firstName = "Mateus";
        const lastName = "Freitas";
        const fullName = `${firstName} ${lastName}`

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type("mateus@testing.com");
        cy.get('#ticket-quantity').select("2");
        cy.get('#vip').check();
        cy.get('#friend').check();
        cy.get('#requests').type("Beer");

        cy.get(".agreement p").should(
            "contain", `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get('#agree').click();
        cy.get('#signature').type(fullName);

        cy.get("button[type='submit']").as("submitButton").should("not.be.disabled");

        cy.get("button[type='reset']").click();
        
        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory field using support command", () => {
        const customer = {
            firstName: "Joao",
            lastName: "Silva",
            email: "joao@silva.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']").as("submitButton").should("not.be.disabled");
        cy.get('#agree').uncheck();
        cy.get("@submitButton").should("be.disabled");
    });
});